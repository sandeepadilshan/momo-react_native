'use strict'
import React, { PropTypes, Component } from 'react'
import {
    View, Dimensions, StyleSheet, Image, Text, TouchableOpacity,Button,
} from 'react-native'

import colors from './../../styles/colors'
import Icon from "react-native-vector-icons/FontAwesome";

const { width } = Dimensions.get('window')
const prdWidth = (width - 45) / 2

class CartItemList extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{padding: 10}}>

                <View style={styles.containerRow} >

                    <View style={styles.buttonContainer}>
                        <Image style={ styles.productImage } source={{ uri: this.props.imageUri }} />
                    </View>

                    <View style={styles.buttonContainer}>
                        <Text style={ styles.name } >
                            <Icon style={styles.icon} name='minus' size={18}/>
                               1
                            <Icon style={styles.icon} name='plus' size={18}/>
                        </Text>
                        <Text style={ styles.name } ellipsizeMode='tail' numberOfLines={2}>
                            { this.props.name }
                        </Text>
                        <Text>${ this.props.prize }</Text>
                        <View style={ styles.promotionHolder }>
                            <Text style={[ styles.name, { textDecorationLine: 'line-through' } ]}>${ this.props.regularPrize }</Text>
                            <Text style={ styles.name }> | { this.props.discountPercent }% off</Text>
                        </View>
                    </View>

                </View>

            </View>


        )
    }
}

const styles = StyleSheet.create({
    containerRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        marginTop:0,
        flex: 1,
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#808069',
        padding: 10,
        margin:10
    },
    holder: {
        width: prdWidth,
        height: prdWidth + 110
    },
    productImage: {
        width: prdWidth,
        height: prdWidth,
        borderWidth: 0.5,
        borderColor: colors.bd_input
    },
    name: {
        marginTop: 6,
        marginBottom: 6,
        color: colors.txt_description,
        fontSize:15
    },
    promotionHolder: {
        flexDirection: 'row'
    }
})

module.exports = CartItemList
