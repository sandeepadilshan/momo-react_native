'use strict'
import React, {Component} from 'react'
import {
    Text,
    Image,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native'

import Container from './../resources/components/Container'

import colors from './../resources/styles/colors'

import Icon from 'react-native-vector-icons/FontAwesome'
import Header from './../resources/components/Header'

import homeData from './../data/home'

var {height, width} = Dimensions.get('window');
const initWidth = width;
const initHeight = initWidth * (500/900)

class About extends Component {
    static navigationOptions = {
        drawerLabel: 'About',
        drawerIcon: ({ tintColor }) => (
            <Icon style={styles.icon} name='address-card' size={20}/>
        ),
    };

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Container>
                <View style={{ width: 100, height: 30 }}/>
                <Header navigation={this.props.navigation} title="About"/>

                <ScrollView>
                    <View style={styles.viewCenter}>
                        <Image
                            style={{width: 120, height: 60,justifyContent: 'center', alignItems: 'center',}}
                            source={{uri: 'https://www.pngfind.com/pngs/m/87-878363_fashion-week-alabama-logo-creative-fashion-logo-design.png'}}
                        >
                            <Text style={{backgroundColor: 'transparent'}}>Logo</Text>
                        </Image>
                    </View>
                    <View style={styles.viewCenter}>
                        <Text style={{backgroundColor: 'transparent', fontSize:45}}>About</Text>
                    </View>
                    <View style={styles.viewCenter}>
                        <Image
                            style={{width: initWidth, height: 200,justifyContent: 'center', alignItems: 'center',}}
                            source={{uri: 'https://nadunbaduge.com/wp-content/uploads/2018/05/37I6797-3-copy.jpg'}}
                        >
                        </Image>
                    </View>
                    <View style={styles.viewCenter}>
                        <Text style={{margin: 10,backgroundColor: 'transparent', fontSize:15}}>
                            {'\n'}
                            Our mission is to empower women globally and to make them confident in their own sense of fashion and outfit. Kelly Felder is a bold, progressive fashion brand inspired by real realistic aims and objectives. Everything we design, create and promote is influenced by our customer along with global influences like social media, street style, and popular culture, creating a destination that delivers and encompasses everything it means to be woman on the go in the world today.
                            {'\n'}{'\n'}
                            OUR PRODUCT
                            {'\n'}{'\n'}
                            We create looks designed by in-house talent that's made to equip millennial women with the fashion they need to empower themselves to face all elements of life. And since our aim is all about empowerment, we make it easy for everyone by making our product affordable. It’s not just fast fashion - its rapid fashion. We constantly provide our angels with new, alluring collections and styles in the form of ready-to-go outfits. Established in 2007, Kelly Felder has risen through the extremely competitive ranks in the Fashion industry of the Island to be the leading fashion presence in the Island that single handedly caters to any and all fashion- related cravings of every woman.</Text>
                    </View>
                </ScrollView>
            </Container>
        )
    }


}

const styles = StyleSheet.create({
    viewCenter:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: colors.bg_header
    },
    headerSub: {
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logo: {
        width: 64,
        height: 28,
        resizeMode: 'center'
    },
    icoSearch: {
        color: colors.txt_description,
        marginRight: 5
    },
    btnSearchHolder: {
        padding: 15,
        paddingTop: 0
    },
    btnSearch: {
        borderColor: colors.bd_input,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 8,
        backgroundColor: colors.bg1,
        borderWidth: 1,
        borderRadius: 5
    },
    btnSearchTitle: {
        color: colors.txt_description,
        fontSize: 16
    },
    btnDeals: {
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 0.5
    },
    icoDeals: {
        color: colors.txt_description,
        marginRight: 10
    },
    section_title:{
        fontSize: 18,
        fontWeight: '600',
        padding: 20
    }
})

module.exports = About
