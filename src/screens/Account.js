'use strict'
import React, {Component} from 'react'
import {
    Text,
    Image,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Modal,Alert,TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import Container from './../resources/components/Container'
import ListPanel from './../resources/components/ListPanel'

import GridProductThumb from './../resources/components/product/GridProductThumb'
import Grid from './../resources/components/Grid'
import colors from './../resources/styles/colors'
import Header from './../resources/components/Header'

import homeData from './../data/home'

import Utils from './../resources/helpers/Utils'
import SwiperProductThumb from "./../resources/components/product/SwiperProductThumb";
import Swiper from "./../resources/components/Swiper";

var {height, width} = Dimensions.get('window');
const initWidth = width;
const initHeight = initWidth * (500/900)

class Account extends Component {
    static navigationOptions = {
        drawerLabel: 'Featured',
        drawerIcon: ({ tintColor }) => (
            <Icon style={styles.icon} name='list' size={16}/>
        ),
    };

    state = {
        pressedProductItem: null,
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }
    constructor(props) {
        super(props)
    }

    checkMyRecentItems(){
        const grid_myRecentList = {
            title: 'Recently Viewed',
            description: 'Best sale today',
            items: [
                {
                    id: 1,
                    name: 'Earing',
                    imageUri: 'https://c1.staticflickr.com/3/2337/1637193093_f5661532b0_z.jpg?zz=1',
                    prize: 80,
                    regularPrize: 100.00,
                    discountPercent: 20
                },
                {
                    id: 2,
                    name: 'iPhone 7 Plus 6S Plus 6 Plus Screen Protector',
                    imageUri: 'https://c1.staticflickr.com/3/2272/2102183091_fb38bb3822.jpg',
                    prize: 7.99,
                    regularPrize: 29.99,
                    discountPercent: 73
                },
                {
                    id: 3,
                    name: 'iPhone 7 Plus 6S Plus 6 Plus Screen Protector,',
                    imageUri: 'https://c1.staticflickr.com/7/6109/6311668225_673b9e1e3c_z.jpg',
                    prize: 30.10,
                    regularPrize: 39.99,
                    discountPercent: 25
                },
                {
                    id: 4,
                    name: 'Skip Hop Moby Bath Tear-Free Waterfall Rinser, Blue',
                    imageUri: 'https://c2.staticflickr.com/4/3311/3670965640_da4a67be4f_z.jpg?zz=1',
                    prize: 8.00,
                    regularPrize: 10.00,
                    discountPercent: 20
                }
            ]
        }

        return grid_myRecentList;
    }

    render() {
        console.log(this.checkMyRecentItems());
        return (
            <Container>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={{marginTop: 22}}>
                        <View>
                        <View style={{marginTop: 22,  margin:5}}></View>
                            <View>
                            <View style={{marginLeft:15}}>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                               <Icon style={styles.icon} name='close' size={20}/>  
                            </TouchableHighlight>
                            </View>
                            <View style={{marginTop: 22}}></View>
                            <Image 
                            source={{uri: 'https://nadunbaduge.com/wp-content/uploads/2018/05/37I6797-3-copy.jpg'}}
       style={{width: 400, height: 400}} />
                             <Text>{this.state.pressedProductItem}</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
                <View style={{ width: 100, height: 30 }}/>
                <Header navigation={this.props.navigation} title="Account"/>
                <ScrollView>
                    {this._renderGridList(this.checkMyRecentItems())}
                </ScrollView>
            </Container>


        )
    }

    _renderGridList(data) {
        return (
            <ListPanel onPressSeeAll={() => this._pressSeeAllProducts({navBarTitle: data.title})} title={data.title} description={data.description}>
                <Swiper>
                    {
                        data.items.map((item, idx) => {
                            return <SwiperProductThumb onPress={() => this._pressProduct(item.id)} key={idx} { ...item }/>
                        })
                    }
                </Swiper>
            </ListPanel>
        )
    }

    _pressProduct(data){
        console.log(data);
        this.setState({pressedProductItem:data});
        this.setModalVisible(true);
        // Utils.showMessage('You clicked on a product');
        // return (
        //     <View style={{marginTop: 22}}>
        //
        //
        //         <TouchableHighlight
        //             onPress={() => {
        //                 this.setModalVisible(true);
        //             }}>
        //             <Text>Show Modal</Text>
        //         </TouchableHighlight>
        //     </View>
        // );
    }

    _pressSeeAllProducts(){
        Utils.showMessage('You clicked to see all products')
    }

}

const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.bg_header
    },
    headerSub: {
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logo: {
        width: 64,
        height: 28,
        resizeMode: 'center'
    },
    icoSearch: {
        color: colors.txt_description,
        marginRight: 5
    },
    btnSearchHolder: {
        padding: 15,
        paddingTop: 0
    },
    btnSearch: {
        borderColor: colors.bd_input,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 8,
        backgroundColor: colors.bg1,
        borderWidth: 1,
        borderRadius: 5
    },
    btnSearchTitle: {
        color: colors.txt_description,
        fontSize: 16
    },
    btnDeals: {
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 0.5
    },
    icoDeals: {
        color: colors.txt_description,
        marginRight: 10
    },
    section_title:{
        fontSize: 18,
        fontWeight: '600',
        padding: 20
    }
})

module.exports = Account
