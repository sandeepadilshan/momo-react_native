'use strict'
import React, {Component} from 'react'
import {
    Text,
    Image,
    View,
    StyleSheet,
    TouchableOpacity,Button,
    ScrollView,
    Dimensions
} from 'react-native'

import Container from './../resources/components/Container'

import colors from './../resources/styles/colors'

import Icon from 'react-native-vector-icons/FontAwesome'
import Header from './../resources/components/Header'

import homeData from './../data/home'
import ListPanel from "../resources/components/ListPanel";
import Swiper from "../resources/components/Swiper";
import SwiperProductThumb from "../resources/components/product/SwiperProductThumb";
import Grid from "../resources/components/Grid";
import CartItemList from "../resources/components/cartItems/CartItemList";

var {height, width} = Dimensions.get('window');
const initWidth = width;
const initHeight = initWidth * (500/900)

class MyCart extends Component {
    static navigationOptions = {
        drawerLabel: 'My Cart',
        drawerIcon: ({ tintColor }) => (
            <Icon style={styles.icon} name='shopping-cart' size={20}/>
        ),
    };

    constructor(props) {
        super(props)
    }
    
    onPressApplyDiscount(){
        console.log("onPressApplyDiscount");
    }

    onPressApplyGiftCard(){
        console.log("Info","onPressApplyGiftCard");
    }

    onPressCheckOut(){
        console.log("Info","onPressCheckOut");
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{  width: 100, height: 30 }} />
                <View style={{ flex: 0.8  }} >
                <Header navigation={this.props.navigation} title="My Cart"/>
                <View style={{height:60}}>
                    <View style={styles.containerRow}>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                style={styles.button} onPress={this.onPressApplyDiscount}
                            >
                                <Text> Apply Discount</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                style={styles.button}onPress={this.onPressApplyGiftCard}
                            >
                                <Text> Add Gift Card </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View>
                    <ScrollView>
                        {this._renderCartItemList(homeData.bags)}
                    </ScrollView>
                </View>
                </View>
                <View style={{ marginTop:50, width: 100, height: 30 }} />
                <View style={{ flex: 0.2,backgroundColor:'#eaeaea' }} >
                    <View style={styles.containerRow}>
                        <View style={{marginRight:-10}}>
                            <Text>1 item</Text>
                        </View>
                        <View style={{marginLeft:80}}>
                            <Text> Total: <Text style={{fontWeight: "bold"}}> $437.00</Text></Text>
                        </View>
                    </View>
                    <View
                        style={{
                            borderBottomColor: '#808069',
                            borderBottomWidth: 1,
                            marginTop:10
                        }}
                    />
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={{alignItems: 'center',
                            backgroundColor: '#808069', padding:10, marginTop:5, margin:30,height:40}}
                                          onPress={this.onPressCheckOut}>
                            <Text style={{fontSize:25,colors:'#fafafa',marginTop:-5}}>Checkout</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }


    _renderCartItemList(data) {
        return (
            <View>
                    {
                        data.items.map((item, idx) => {
                            return <CartItemList onPress={() => this._pressProduct(item.id)} key={idx} { ...item }/>
                        })
                    }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    containerRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
    },
    buttonText: {
        alignItems: 'center',
        backgroundColor: '#808069',

    },
    button: {
        alignItems: 'center',
        backgroundColor: '#808069',
        padding: 10,
        margin:10
    },
    viewCenter:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: colors.bg_header
    },
    headerSub: {
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logo: {
        width: 64,
        height: 28,
        resizeMode: 'center'
    },
    icoSearch: {
        color: colors.txt_description,
        marginRight: 5
    },
    btnSearchHolder: {
        padding: 15,
        paddingTop: 0
    },
    btnSearch: {
        borderColor: colors.bd_input,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 8,
        backgroundColor: colors.bg1,
        borderWidth: 1,
        borderRadius: 5
    },
    btnSearchTitle: {
        color: colors.txt_description,
        fontSize: 16
    },
    btnDeals: {
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 0.5
    },
    icoDeals: {
        color: colors.txt_description,
        marginRight: 10
    },
    section_title:{
        fontSize: 18,
        fontWeight: '600',
        padding: 20
    }
})

module.exports = MyCart
